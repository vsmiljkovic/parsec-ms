import filecmp
import datetime

time_nondettm_dir       = './time-nondet-tm/'
time_nondetadhoc_dir    = './time-nondet-adhoc/'
time_dettm_dir          = './time-det-tm/'
time_dettmadhoc_dir     = './time-det-tmadhoc/'
time_detadhoc_dir       = './time-det-adhoc/'


benchmarks = {
 'fluidanimate-tm': ' 5 in_35K.fluid out.fluid',
 'fluidanimate-adhoc': ' 5 in_35K.fluid out.fluid',
}

threads = [1, 2, 4]

nb_iterations = 1
now = datetime.datetime.now()

out = now.strftime("%Y-%m-%d-%H-%M")
out_all    = out + "_all"       # in this file we'll put all the results
out_parsed = out + "_parsed"    # in this file we'll put parsed results


def measure_time(name, preload, params, time_dir):
  print "echo", name , params, ">", out_parsed

  for nb_threads in threads:
    print "echo -n > tmp" # an empty file tmp
    print "echo", name, str(nb_threads), params, ">>", out_all 
    for i in range(nb_iterations):
      print "{ time " + preload + " ./" + name + " " + str(nb_threads) + params + "; } 2>> tmp"
    print "cat tmp >>", out_all
 
    # take minutes and seconds as substrings and calculate time only in seconds
    print "grep real tmp |",
    print "awk '{ " + \
               "im = index($2,\"m\");" + \
               "is = index($2,\"s\");" + \
               "m  = substr($2,1,im-1);" + \
               "s  = substr($2,im+1,is-im-1);" + \
               "t += m*60+s } END {print t/NR;}'",
    print ">>", out_parsed

# the files created for measuring time (date_time_all and date_time_parsed) will be in time_dir
  print "mv", out_all, out_parsed, time_dir
#  print "rm tmp"


# run deterministically, with LD_PRELOAD
name = "fluidanimate-tm"
params = benchmarks[name]
preload = ""
measure_time(name, preload, params, time_nondettm_dir)

name = "fluidanimate-adhoc"
params = benchmarks[name]
preload = ""
measure_time(name, preload, params, time_nondetadhoc_dir)

name = "fluidanimate-tm"
params = benchmarks[name]
preload = "LD_PRELOAD=/home/vesna/determ/deterministic/libt.so.1.0.1"
measure_time(name, preload, params, time_dettm_dir)

name = "fluidanimate-tm"
params = benchmarks[name]
preload = "LD_PRELOAD=/home/vesna/determ/deterministic/libt-adhoc-100.so.1.0.1"
measure_time(name, preload, params, time_dettmadhoc_dir)

name = "fluidanimate-adhoc"
params = benchmarks[name]
preload = "LD_PRELOAD=/home/vesna/determ/deterministic/libt-adhoc.so.1.0.1"
measure_time(name, preload, params, time_detadhoc_dir)
