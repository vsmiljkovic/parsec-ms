#./bin/parsecmgmt -a build -p streamcluster -c gcc-pthreads
#./bin/parsecmgmt -a build -p streamcluster -c gcc-tm
#./bin/parsecmgmt -a build -p fluidanimate -c gcc-pthreads
#./bin/parsecmgmt -a build -p fluidanimate -c gcc-tm

#./bin/parsecmgmt -a run -p fluidanimate -c gcc-pthreads -n 1 -i native

threads=(1 2 4)

#for i in "${threads[@]}"
#do
#  echo $i
#  ./bin/parsecmgmt -a run -p fluidanimate -c gcc-pthreads -n $i -i simsmall
#done



# ------------- clean fluidanimate ---------------#

#./bin/parsecmgmt -a uninstall -p fluidanimate  -c gcc-tm

#-------------- build fluidanimate ---------------#

./bin/parsecmgmt -a build -p fluidanimate -c gcc-tm

#---------------- run fluidanimate ---------------#

cd ~/determ/parsec/pkgs/apps/fluidanimate/run
for i in "${threads[@]}"
do
#  ./bin/parsecmgmt -a run -p fluidanimate -c gcc-tm -n $i -i simsmall
  ../inst/amd64-linux.gcc-tm/bin/fluidanimate $i 5 in_35K.fluid out.fluid
  LD_PRELOAD=/home/vesna/determ/deterministic/libt.so.1.0.1 ../inst/amd64-linux.gcc-tm/bin/fluidanimate $i 5 in_35K.fluid out.fluid
  LD_PRELOAD=/home/vesna/determ/deterministic/libt-adhoc.so.1.0.1 ../inst/amd64-linux.gcc-tm/bin/fluidanimate $i 5 in_35K.fluid out.fluid

done
cd ~/determ/parsec


# ------------- clean streamcluster ---------------#

#./bin/parsecmgmt -a uninstall -p streamcluster -c gcc-tm

#-------------- build streamcluster ---------------#

./bin/parsecmgmt -a build -p streamcluster -c gcc-tm

#-------------- run streamcluster ---------------#


cd ~/determ/parsec/pkgs/kernels/streamcluster/run
for i in "${threads[@]}"
do
#  ./bin/parsecmgmt -a run -p streamcluster -c gcc-tm -n $i -i simlarge

  ../inst/amd64-linux.gcc-tm/bin/streamcluster 10 20 128 16384 16384 1000 none output.txt $i
# LD_PRELOAD=/home/vesna/determ/deterministic/libt.so.1.0.1 ../inst/amd64-linux.gcc-tm/bin/streamcluster 10 20 128 16384 16384 1000 none output.txt $i
 # LD_PRELOAD=/home/vesna/determ/deterministic/libt-adhoc.so.1.0.1 ../inst/amd64-linux.gcc-tm/bin/streamcluster 10 20 128 16384 16384 1000 none output.txt $i
done
cd ~/determ/parsec

